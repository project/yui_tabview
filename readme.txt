This module requires litte bit advanced users. But give a try.

VERY IMPORTANT on DISABLING THE MODULE 
--------------------------------------
Be sure that you remove all the usage of the build_tabview function from your site. 

How to enable and use the module
--------------------------------
- Extract the tabview to your module directory
- Enable it from Site Building->Modules (admin/build/modules)
- Visit the sample page at tabview/sample (http://localhost/?q=tabview/sample or http://localhost/tabview/sample)


You can use it directly from your php code or you can just add a block to your site.

To pleace a block on your site just do the followings

- Goto Site Building->Blocks admin/build/block
- Add a new block
- Place the following code to the "Block body:" and change the 'Tab One Label' and 'Tab One Content' 
  with your data. You can add or remove $nodes[] lines as you require.
 
  <?php
    $nodes[] = (object)array('nav' => 'Tab One Label', 'content' => 'Tab One Content');
    $nodes[] = (object)array('nav' => 'Tab Two Label', 'content' => 'Tab Two Content');
    $nodes[] = (object)array('nav' => 'Tab Three Label', 'content' => 'Tab Three Content');
    $output = build_tabview("test",$nodes);
    return $output;
  ?>

- Pleace your block on your site.

 